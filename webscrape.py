# For importing HTML data
from lxml import html
import requests

# For formatting money
from decimal import Decimal, ROUND_DOWN

import statistics
import numpy as np



def ebayScrape(fullURL):
    print('-----------------')
    print('Ebay price stats:')
    # Pull data from website into a tree structure
    # Still need to learn more about how this tree structure works
    page = requests.get(fullURL)
    tree = html.fromstring(page.content)

    # Create an array of prices from tree data
    prices = tree.xpath('//span[@class="bold bidsold"]/text()')

    # Get prices list length
    array_length = len(prices)

    errCount = 0
    cnt1 = 0
    pricesFloat = []
    # Build an array of prices
    for i in range(array_length):
        index1 = prices[i].find('$') + 1
        prices[i] = prices[i][index1:]
        prices[i] = prices[i].replace(" ","")
        prices[i] = prices[i].replace(",","")
        try:
            float(prices[i])
            pricesFloat.append(float(prices[i]))
            cnt1 = cnt1 + 1
        except:
            errCount = errCount + 1

    # Send list of floats to 'resultStats'
    resultStats(pricesFloat)


def cListScrapeMil(fullURL):
    print('-----------------')
    print('Craigslist Milwaukee Price Stats:')
    # Pull data from website into a tree structure
    # Still need to learn more about how this tree structure works
    page = requests.get(fullURL)
    tree = html.fromstring(page.content)

    # Create an array of prices from tree data
    prices = tree.xpath('//span[@class="result-price"]/text()')
    # Get prices list length
    array_length = len(prices)

    errCount = 0
    cnt1 = 0
    pricesFloat = []
    # Build an array of prices
    for i in range(array_length):
        index1 = prices[i].find('$') + 1
        prices[i] = prices[i][index1:]
        prices[i] = prices[i].replace(" ","")
        prices[i] = prices[i].replace(",","")
        try:
            float(prices[i])
            pricesFloat.append(float(prices[i]))
            cnt1 = cnt1 + 1
        except:
            errCount = errCount + 1

    # Send list of floats to 'resultStats'
    resultStats(pricesFloat)

def cListScrapeChi(fullURL):
    print('-----------------')
    print('Craigslist Chicago Price Stats:')
    # Pull data from website into a tree structure
    # Still need to learn more about how this tree structure works
    page = requests.get(fullURL)
    tree = html.fromstring(page.content)

    # Create an array of prices from tree data
    prices = tree.xpath('//span[@class="result-price"]/text()')
    # Get prices list length
    array_length = len(prices)

    errCount = 0
    cnt1 = 0
    pricesFloat = []
    # Build an array of prices
    for i in range(array_length):
        index1 = prices[i].find('$') + 1
        prices[i] = prices[i][index1:]
        prices[i] = prices[i].replace(" ","")
        prices[i] = prices[i].replace(",","")
        try:
            float(prices[i])
            pricesFloat.append(float(prices[i]))
            cnt1 = cnt1 + 1
        except:
            errCount = errCount + 1

    # Send list of floats to 'resultStats'
    resultStats(pricesFloat)


def resultStats(pricesFloat):
    pricesFloat = sorted(pricesFloat)
    priceArray = np.asarray(pricesFloat)
    
    # Trim low side outliers
    meanPrice = sum(priceArray)/len(priceArray)
    low15 = 0.25*meanPrice
    priceArray = priceArray[priceArray > low15]
    print(priceArray)
    
    # Calculate some states
    meanPrice = sum(priceArray)/len(priceArray)
    medianPrice = priceArray[round(len(priceArray)/2)]
    standardDev = statistics.stdev(priceArray)

    # Rounding
    meanPrice = Decimal(str(meanPrice)).quantize(Decimal('.01'), rounding=ROUND_DOWN)
    standardDev = Decimal(str(standardDev)).quantize(Decimal('.01'), rounding=ROUND_DOWN)
    
    # Printing
    print('Median Price: ${}'.format(medianPrice))
    print('Mean price: ${}'.format(meanPrice))
    print('Standard Deviation: ${}'.format(standardDev))
    print('Number of samples: {}'.format(len(priceArray)))