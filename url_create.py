import webbrowser

# Functions used to generate urls for respective sites
def ebay_url(searchString):
    # Ebay search formatting
    cSearchString = searchString.replace(" ", "+")
    start_url = 'https://www.ebay.com/sch/i.html?_nkw='
    end_url = '&_in_kw=1&_ex_kw=&_sacat=0&LH_Sold=1&_udlo=&_udhi=&LH_BIN=1&LH_ItemCondition=4&_samilow=&_samihi=&_sadis=15&_stpos=53215-3940&_sargn=-1%26saslc%3D1&_salic=1&_sop=12&_dmd=1&_ipg=100&LH_Complete=1&_fosrp=1'
    full_url = (start_url + cSearchString + end_url)
    return full_url

def clist_url_mil(searchString):
    # Milwaukee Craigslist search formatting
    cSearchString = searchString.replace(" ", "+")
    start_url = 'https://milwaukee.craigslist.org/search/sss?query='
    end_url = '&srchType=T'
    full_url = (start_url + cSearchString + end_url)
    return full_url

def clist_url_chi(searchString):
    # Chicago Craigslist search formatting
    cSearchString = searchString.replace(" ", "+")
    start_url = 'https://chicago.craigslist.org/search/sss?query='
    end_url = '&srchType=T'
    full_url = (start_url + cSearchString + end_url)
    return full_url

def facebook_url_mil(searchString):
    # Milwaukee Facebook search formatting
    cSearchString = searchString.replace(" ", "%20")
    start_url = 'https://www.facebook.com/marketplace/milwaukee/search/?query='
    end_url = '&vertical=C2C&sort=BEST_MATCH'
    full_url = (start_url + cSearchString + end_url)
    return full_url